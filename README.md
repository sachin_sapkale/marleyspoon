# Marley Spoon Android Challenge task
Android Task for Marley Spoons 

I've used the following while implementing this challenge task :-

- Have written entire code in Java language.
- Used Contentful sdk along with rxjava to consume api for client app.
- Used Picasso for loading images into view.
- Implemented a MVVM model in order to decouple modules for more flexibility.
- Used standard method to navigate between fragments.
- Added a simple unit test case to check contenful sdk and its response.

**PS: I've added space_id and access_token in app's build.gradle**