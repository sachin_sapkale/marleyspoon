package com.marleyspoon.android.activity;

import android.os.Bundle;

import com.marleyspoon.android.R;
import com.marleyspoon.android.base.BaseActivity;
import com.marleyspoon.android.model.Recipe;
import com.marleyspoon.android.view.singlerecipe.RecipeDetailsFragment;
import com.marleyspoon.android.view.recipelist.RecipeListFragment;

public class MainActivity extends BaseActivity implements RecipeListFragment.RecipeDetailListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecipeListFragment recipeListFragment = new RecipeListFragment();
        recipeListFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.container,recipeListFragment).commit();
    }


    @Override
    public void onDetailRecipeClickListener(Recipe recipeModel) {
        RecipeDetailsFragment recipeDetailsFragment = RecipeDetailsFragment.newInstance(recipeModel);
        recipeDetailsFragment.show(getSupportFragmentManager(), "recipeBottomSheet");
    }
}