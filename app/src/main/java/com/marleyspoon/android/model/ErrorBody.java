package com.marleyspoon.android.model;

public class ErrorBody {

    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
