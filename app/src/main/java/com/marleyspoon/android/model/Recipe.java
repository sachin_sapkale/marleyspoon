package com.marleyspoon.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.contentful.java.cda.CDAAsset;
import com.contentful.java.cda.CDAEntry;
import com.contentful.java.cda.image.ImageOption;

import java.util.ArrayList;

public class Recipe implements Parcelable {

    public String title;
    public String description;
    public String imageUrl;
    public String chefName;
    public ArrayList<String> tags;

    public Recipe(CDAEntry entry) {
        CDAAsset photo = entry.getField("photo");
        CDAEntry chef = entry.getField("chef");

        title = (entry.getField("title"));
        imageUrl = (photo != null ? photo.urlForImageWith(ImageOption.https()) : "");
        description = (entry.getField("description"));
        chefName = (chef != null ? chef.getField("name") : "");
        if (entry.getField("tags") != null) {
            ArrayList<CDAEntry> originalTags = entry.getField("tags");
            ArrayList<String> convertTags = new ArrayList<>();
            for (CDAEntry e : originalTags) {
                convertTags.add(e.getField("name"));
            }
            tags = convertTags;
        }
    }

    protected Recipe(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.imageUrl = in.readString();
        this.chefName = in.readString();
        this.tags = in.createStringArrayList();
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.imageUrl);
        dest.writeString(this.chefName);
        dest.writeStringList(this.tags);
    }
}
