package com.marleyspoon.android.controller;

import android.app.Application;
import android.content.Context;

import com.contentful.java.cda.CDAClient;
import com.marleyspoon.android.network.CDAClientInstanceProvider;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private Scheduler scheduler;

    private static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public static App create(Context context) {
        return App.get(context);
    }

    public CDAClient getCDAClient() {
        return CDAClientInstanceProvider.get();
    }

    public void resetCDAClient() {
        CDAClientInstanceProvider.reset();
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }
        return scheduler;
    }
}
