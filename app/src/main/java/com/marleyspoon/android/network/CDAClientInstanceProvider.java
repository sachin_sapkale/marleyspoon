package com.marleyspoon.android.network;

import com.contentful.java.cda.CDAClient;
import com.marleyspoon.android.BuildConfig;

public class CDAClientInstanceProvider {
  private static CDAClient instance;

  private static final Object LOCK = new Object();

  private CDAClientInstanceProvider() {
    throw new AssertionError();
  }

  @SuppressWarnings("ConstantConditions")
  public static CDAClient get() {
    synchronized (LOCK) {
      if (instance == null) {
        instance = CDAClient.builder()
            .setSpace(BuildConfig.SPACE_ID)
            .setToken(BuildConfig.ACCESS_TOKEN)
            .build();
      }

      return instance;
    }
  }

  public static void reset() {
    synchronized (LOCK) {
      instance = null;
    }
  }
}
