package com.marleyspoon.android.view.recipelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.contentful.java.cda.CDAArray;
import com.contentful.java.cda.CDAEntry;
import com.contentful.java.cda.CDAResource;
import com.marleyspoon.android.R;
import com.marleyspoon.android.base.BaseFragment;
import com.marleyspoon.android.model.ErrorBody;
import com.marleyspoon.android.model.Recipe;
import com.marleyspoon.android.view.adapter.RecipeListAdapter;
import com.marleyspoon.android.viewModel.RecipeViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class RecipeListFragment extends BaseFragment {
    private RecipeViewModel recipeViewModel;
    private RecipeDetailListener recipeDetailListener;

    @BindView(R.id.recipeListRecyclerView) RecyclerView recipeListRecyclerView;
    @BindView(R.id.progressCircular) ProgressBar progressCircular;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        bindView(view);
        recipeViewModel = new ViewModelProvider(this).get(RecipeViewModel.class);
        recipeViewModel.getScreenMutableLiveData().observe(activity, cdaArrayObserver);
        recipeViewModel.getErrorBodyMutableLiveData().observe(activity, errorBodyObserver);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressCircular.setVisibility(View.VISIBLE);
        recipeViewModel.fetchRecipeList(mcontext);
    }

    private Observer<CDAArray> cdaArrayObserver = new Observer<CDAArray>() {
        @Override
        public void onChanged(CDAArray cdaArray) {
            progressCircular.setVisibility(View.GONE);
            ArrayList<Recipe> recipeModelArrayList = new ArrayList<>();
            for (CDAResource resource : cdaArray.items()) {
                CDAEntry entry = (CDAEntry) resource;
                if (entry != null) {
                    Recipe recipeModel = new Recipe(entry);
                    recipeModelArrayList.add(recipeModel);
                }
            }
            if(recipeModelArrayList != null && recipeModelArrayList.size()>0){
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext);
                RecipeListAdapter recipeListAdapter = new RecipeListAdapter(mcontext, recipeModelArrayList);
                recipeListRecyclerView.setVisibility(View.VISIBLE);
                recipeListRecyclerView.setLayoutManager(linearLayoutManager);
                recipeListRecyclerView.setAdapter(recipeListAdapter);
                recipeListAdapter.setOnRecipeClickListner(new RecipeListAdapter.ClickListener() {
                    @Override
                    public void onMenuClickListener(int position) {
                        recipeDetailListener.onDetailRecipeClickListener(recipeModelArrayList.get(position));
                    }
                });
            }else{
                ErrorBody errorBody = new ErrorBody();
                errorBody.setDescription("Something Went Wrong");
                errorBodyObserver.onChanged(errorBody);
            }

        }
    };

    private Observer<ErrorBody> errorBodyObserver = new Observer<ErrorBody>() {
        @Override
        public void onChanged(ErrorBody errorBody) {
            progressCircular.setVisibility(View.GONE);
            Toast.makeText(mcontext, errorBody.getDescription(), Toast.LENGTH_SHORT).show();
        }
    };

    public void setListener(RecipeDetailListener listener) {
        recipeDetailListener = listener;
    }

    public interface RecipeDetailListener {
        void onDetailRecipeClickListener(Recipe recipeModel);
    }
}
