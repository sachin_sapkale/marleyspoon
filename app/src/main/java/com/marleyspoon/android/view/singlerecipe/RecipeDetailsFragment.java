package com.marleyspoon.android.view.singlerecipe;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.marleyspoon.android.R;
import com.marleyspoon.android.model.Recipe;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecipeDetailsFragment extends BottomSheetDialogFragment {

    @BindView(R.id.recipeTitleTextView) TextView recipeNameTextView;

    @BindView(R.id.recipeDescTextView) TextView recipeDescriptionTextView;

    @BindView(R.id.recipeChefNameTextView) TextView recipeChefNameTextView;

    @BindView(R.id.recipeTagsContainer) LinearLayout recipeTagsContainer;

    @BindView(R.id.recipeImageView) ImageView recipeImageView;

    private Recipe recipeDetails;
    private static String KEY_RECIPE = "recipe";

    public static RecipeDetailsFragment newInstance(Recipe recipeModel){
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_RECIPE,recipeModel);
        RecipeDetailsFragment recipeDetailsFragment = new RecipeDetailsFragment();
        recipeDetailsFragment.setArguments(bundle);
        return recipeDetailsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL,R.style.BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_details, container, false);
        ButterKnife.bind(this,view);
        recipeDetails = getArguments().getParcelable(KEY_RECIPE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recipeNameTextView.setText(recipeDetails.title);
        recipeDescriptionTextView.setText(recipeDetails.description);
        Picasso.get().load(recipeDetails.imageUrl).into(recipeImageView);

        if(!TextUtils.isEmpty(recipeDetails.chefName)) {
            recipeChefNameTextView.setText(getString(R.string.chef_name,recipeDetails.chefName));
        }else {
            recipeChefNameTextView.setVisibility(View.GONE);
        }

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        if(recipeDetails.tags !=null) {
            for (String c : recipeDetails.tags) {
              View inflaterView = layoutInflater.inflate(R.layout.view_tags,null);
              TextView textViewTag = (TextView) inflaterView.findViewById(R.id.recipeTagsTextView);
              textViewTag.setText(c);
              recipeTagsContainer.addView(inflaterView);
            }
        }

        //Used to stretch the bottomsheet completely as a work around
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = (FrameLayout) dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }



    @OnClick(R.id.imageviewToolbarCross)
    void dismissDialog(){
        dismiss();
    }
}
