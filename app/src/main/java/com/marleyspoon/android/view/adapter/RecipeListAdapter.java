package com.marleyspoon.android.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.marleyspoon.android.R;
import com.marleyspoon.android.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<Recipe> recipeMenuList;
    private static ClickListener clickListener;

    public RecipeListAdapter(Context act, ArrayList<Recipe> recipeModelsList) {
        this.context = act;
        this.recipeMenuList = recipeModelsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rec_menu_list_single_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.itemTitle.setText(recipeMenuList.get(position).title);
        Picasso.get().load(recipeMenuList.get(position).imageUrl).into(holder.itemMenuImageView);

    }

    @Override
    public int getItemCount() {
        return (recipeMenuList !=null && recipeMenuList.size()>0)?recipeMenuList.size():0;
    }

    public void setOnRecipeClickListner(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView itemTitle;
        private ImageView itemMenuImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            itemTitle = (TextView) itemView.findViewById(R.id.titleTextView);
            itemMenuImageView = (ImageView) itemView.findViewById(R.id.menuThumbnailImageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onMenuClickListener(getAdapterPosition());
        }
    }
    public interface ClickListener{
        void onMenuClickListener(int position);
    }
}