package com.marleyspoon.android.viewModel;

import android.app.Application;
import android.content.Context;

import com.contentful.java.cda.CDAArray;
import com.contentful.java.cda.CDAEntry;
import com.marleyspoon.android.controller.App;
import com.marleyspoon.android.model.ErrorBody;
import com.marleyspoon.android.utilities.AppConstants;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class RecipeViewModel extends AndroidViewModel {

    MutableLiveData<CDAArray> screenMutableLiveData;
    MutableLiveData<ErrorBody> errorBodyMutableLiveData;
    String RECIPE_CONTENT_TYPE = "recipe";

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        screenMutableLiveData = new MutableLiveData<>();
        errorBodyMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<CDAArray> getScreenMutableLiveData() {
        return screenMutableLiveData;
    }

    public MutableLiveData<ErrorBody> getErrorBodyMutableLiveData() {
        return errorBodyMutableLiveData;
    }

    public void fetchRecipeList(Context context) {
        App appController = App.create(context);
        appController.getCDAClient()
                .observe(CDAEntry.class)
                .where(AppConstants.CONTENT_TYPE_KEY, RECIPE_CONTENT_TYPE)
                .all()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(appController.subscribeScheduler())
                .subscribe(new DisposableSubscriber<CDAArray>() {
                    CDAArray result;

                    @Override
                    public void onComplete() {
                        screenMutableLiveData.setValue(result);
                    }

                    @Override
                    public void onError(Throwable error) {
                        ErrorBody errorBody = new ErrorBody();
                        errorBody.setDescription(error.toString());
                        errorBodyMutableLiveData.setValue(errorBody);
                    }

                    @Override
                    public void onNext(CDAArray cdaArray) {
                        result = cdaArray;
                    }
                });
    }


}
