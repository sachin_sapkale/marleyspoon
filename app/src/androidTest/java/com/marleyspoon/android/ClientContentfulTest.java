package com.marleyspoon.android;

import com.contentful.java.cda.CDAArray;
import com.contentful.java.cda.CDAClient;
import com.contentful.java.cda.CDAEntry;
import com.marleyspoon.android.network.CDAClientInstanceProvider;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;

@RunWith(AndroidJUnit4.class)
public class ClientContentfulTest {
    @Test
    public void checkGetRecipeListMethod() {
        CDAClient cdaClient = CDAClientInstanceProvider.get();
        Assert.assertNotNull(cdaClient);

        CDAArray cdaArray = cdaClient.fetch(CDAEntry.class).all();
        Assert.assertNotNull(cdaArray);
        Assert.assertTrue(cdaArray.items()!=null);
        Assert.assertTrue(cdaArray.items().size()>0);
        Assert.assertNotNull(cdaArray.items().get(0));
    }
}
